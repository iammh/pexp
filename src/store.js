import Vue from 'vue'
import Vuex from 'vuex'
import {AccountService} from "@/api/account";

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    accounts: [],
    transactions: [
        {account: 31373, amount: 100, tag: 'Transport', date: new Date()},
        {account: 31373, amount: 100, tag: 'Transport', date: new Date(-1)},
    ]
  },
  getters: {
      accounts(state) {
          return state.accounts
      },
      transactions(state) {
          return state.transactions
      }
  },
  mutations: {
    accounts(state, account) {
        console.log("Settings accounts")
      state.accounts = account;
    },
    addAccount(state, a) {
      state.accounts.push(a);
    },
    transaction(state, t) {
        state.transactions.push(t)
    }
  },
  actions: {
    GetAccounts({commit}) {
        AccountService.accounts()
            .then((response) => {
                commit('accounts', response.data);
            })
    },
    CreateAccount({commit}, account) {
      AccountService.create(account)
          .then((done) => {
            console.log('Got data from api ', done)
            commit('addAccount', done);
          })
    },
    AddTransaction({commit}, transaction) {
        AccountService.addTransaction(transaction)
            .then((savedTransaction) => {
               commit('transaction', transaction);
            }).catch((err) => {

            });
    }
  }
})

import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import CreateAccount from './views/CreateAccount.vue'
import AddTransaction from './views/AddTransaction.vue'
import AccountDetails from './views/AccountDetails.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    },
    {
      path: '/accounts/create',
      name: 'createaccount',
      component: CreateAccount
    },
    {
        path: '/transactions/add',
        name: 'addTransaction',
        component: AddTransaction
    },
    {
        path: '/accounts/details/:number',
        name: 'details',
        props: true,
        component: AccountDetails
    }
  ]
})

import axios from 'axios';


export const AccountService = {
    accounts() {
        return new Promise((resolve) => {
            resolve({"data": [
                    {currency: 'USD',currencySymbol: '$',id: 1,balance: 200,number: 31373, name: 'Account One', avatar: 'https://cdn4.iconfinder.com/data/icons/basic-interface-overcolor/512/user-512.png'},
                    {currency: 'USD',currencySymbol: '$',id: 1,balance: 2000,number: 202598, name: 'Account Two', avatar: 'https://cdn4.iconfinder.com/data/icons/basic-interface-overcolor/512/user-512.png'},
                    {currency: 'USD',currencySymbol: '$',id: 1,balance: -500,number: 302586, name: 'Account Three', avatar: 'https://cdn4.iconfinder.com/data/icons/basic-interface-overcolor/512/user-512.png'}

                ]});
        });
    },
    create(account) {
        // return axios.post('/', account, {});

        return new Promise((resolve)=> {
            console.log("Resolving");
            setTimeout(()=> {
                account['number'] = Math.round(Math.random()*10000+1);
                account['currency'] = '$';
                account['balance'] = 500;
                account['avatar'] = 'https://cdn4.iconfinder.com/data/icons/basic-interface-overcolor/512/user-512.png';
                resolve(account);
            },3000)

        });
    },
    getOne(number) {
        let acct =  {
            currency: 'USD',
            currencySymbol: '$',
            id: 1,
            balance: 200,
            number: 31373,
            name: 'Account One',
            avatar: 'https://cdn4.iconfinder.com/data/icons/basic-interface-overcolor/512/user-512.png'
        };

        let accts = [
            {currency: 'USD',currencySymbol: '$',id: 1,balance: 200,number: 31373, name: 'Account One', avatar: 'https://cdn4.iconfinder.com/data/icons/basic-interface-overcolor/512/user-512.png'},
            {currency: 'USD',currencySymbol: '$',id: 1,balance: 2000,number: 202598, name: 'Account Two', avatar: 'https://cdn4.iconfinder.com/data/icons/basic-interface-overcolor/512/user-512.png'},
            {currency: 'USD',currencySymbol: '$',id: 1,balance: -500,number: 302586, name: 'Account Three', avatar: 'https://cdn4.iconfinder.com/data/icons/basic-interface-overcolor/512/user-512.png'}

        ];
        console.log("Number ", number);
        return new Promise((resolve,reject) => {
            setTimeout(()=> {
                let i = accts.map((item)=> item.number == number ? item: null)
                console.log("Item s", i);
                let out = i.reduce((m,idn)=> { return m !== null ? m: idn;}, null);
                console.log("Out ", out);
                if(out) {
                    resolve(out);
                }else {
                    reject(new Error("Not found"))
                }

            },2000)
        })
    },
    addTransaction(transaction) {
        return new Promise((resolve )=> {
            resolve(transaction);
        })
    }
}

import '@babel/polyfill'
import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import router from './router'
import store from './store'
import 'roboto-fontface/css/roboto/roboto-fontface.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css'

Vue.config.productionTip = false

Vue.mixin({
    filters: {
        format(value) {

            let amount = value.balance;

            if(value.balance < 0) {
                amount = '<span style="color: red;">'+value.balance+'</span>'
            }
            return '<b>'+value.currencySymbol +':</b> '+ amount;
        },
        formatDate(val) {
            return new Date()
        }
    },
    methods: {
      back() {
        this.$router.go(-1)
      }
    }
})


new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
